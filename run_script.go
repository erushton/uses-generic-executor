package main

import (
	"bytes"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"syscall"
)

func main() {
	info, err := os.Stdin.Stat()
	if err != nil {
		panic(err)
	}

	if info.Mode()&os.ModeCharDevice != 0 || info.Size() <= 0 {
		fmt.Println("The command is intended to work with pipes.")

		return
	}

	output, err := ioutil.ReadAll(os.Stdin)
	if err != nil {
		panic(err)
	}

	jobScript := string(output)

	fmt.Println("The run scripts UID started as ", os.Getuid())
	err = syscall.Setuid(501)
	if err != nil {
		panic(err)
	}
	fmt.Println("The run scripts UID is now ", os.Getuid())

	cmd := exec.Command("bash")
	cmd.Stderr = os.Stderr
	cmd.Stdin = bytes.NewBufferString(jobScript)
	cmd.Stdout = os.Stdout

	err = cmd.Start()
	if err != nil {
		fmt.Println("Failed to start process")

	}

	// Wait for process to finish
	waitCh := make(chan error)
	go func() {
		err := cmd.Wait()
		if _, ok := err.(*exec.ExitError); ok {
			err = errors.New("Error waiting for process to finish")
		}
		waitCh <- err
	}()

	select {
	case err = <-waitCh:
		return

	}
}
